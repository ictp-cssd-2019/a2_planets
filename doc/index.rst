.. A2_planets documentation master file, created by
   sphinx-quickstart on Wed May  8 15:55:18 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to A2_planets's documentation!
======================================

SCOPE
+++++

We are presenting A2PlanetTool a physics teaching tool. Our goal is to impact the teaching-learning community with a set of open source and computing based tools for the physics learning-teaching process. In this release, we have created the graphic simulation for the planetary system, based on Newton's law of gravitation. This graphic simulation can be used as a tool to teach the principles of gravitation theory.

Theoretical framework
+++++++++++++++++++++

Newton's law of universal gravitation, states that every particle attracts every other particle in the universe with a force which is directly proportional to the product of their masses and inversely proportional to the square of the distance between their centers.Equation [1] presents the universal gravitation formula. 
   .. math:: F=G\frac{m_{1}m_{2}}{r^{2}}
      :label: newton's law

Where,
F: is the gravitational force acting between two or more planets
m1 and m2: are the masses of the objects
r: is the distance between the centers of their masses
G: is the gravitational constant. 

How the program works
+++++++++++++++++++++
The user can develop several experiments using variations on the input file system. In this file, should be included information about the initial conditions and parameters of the planet. The initial conditions are position (x,y),acceleration(ax,ay) and velocity (vx,vy). Only one parameter is included in the equations system, mass. 

.. code-block:: bash

	[Global] 

	G = ... 
	... 

	[Panet] 
	name = ... 
	mass = ... 
	x_0 =... 
	y_0 = ... 
	Vx_0 = ... 
	Vy_0 = ... 
	... 

	[Panet] 
	... 

	[Panet] 
	... 

Numerical solution
++++++++++++++++++
A numerical solution of the Equation [1], was given by Verlet integration method. Starting with the updated acceleration component, as a result of the addition of the gravitational forces (see Equation [2]), the model update the velocity and position. Following the Verlet method, were used until the third Taylor expansion components for each equation (position and velocity). Equations [3] and [4] represents the Verlet method for the planets orbit system.

.. math:: a(n+1)=\frac{\Sigma F_{x,i}{}_{i,j}}{m_{i,j}}
   :label: aceleration

.. math:: x_{n+1}=x_{n}+v_{n}*\delta t + \frac12 *a_{n}*\delta t^2 
   :label: position
.. math:: v_{n+1}=v_{n}+\frac12 *\delta t *(a_{n+1}+a{n})
    :label: velocity

Where, 
a:acceleration
v:velocity
n: represents the numerical solution step.
t: time

A Computer algorithm cannot resolve the equations [3] and [4] in a half step (n/2). Instead it was assumed the average of the acceleration between the steps n and n+1. On Equations [5] to [8] are presented the loop for the velocity and position updating in the first simulation step. 

.. math:: v(n+1)=v(n)+\frac {a(n)}2 *\delta t  
   :label: velocity first step
.. math:: a(n+1)=\frac{\Sigma F_{x,i}{}_{i,j}}{m_{i,j}}
   :label: aceleration step
.. math:: v(n+1)+=\frac {a(n+1)}2 *\delta t
   :label: velocity step
.. math:: x(n+1)=x(n)+v(n)*\delta t + \frac12 *a(n)*\delta t 
   :label: postition step



User Ref. Manual
++++++++++++++++

.. autofunction:: a2_planets.animation.animate

.. autofunction:: a2_planets.functions.import_planets

.. autofunction:: a2_planets.functions.change_position

.. autofunction:: a2_planets.functions.change_velocity_partially

.. autofunction:: a2_planets.functions.get_distance

.. autofunction:: a2_planets.functions.forces_fun

.. autofunction:: a2_planets.functions.acceleration_fun

.. autoclass:: a2_planets.planet.Planet



.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
