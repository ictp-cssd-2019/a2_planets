import setuptools
from setuptools import find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="a2_planets",
    version="0.0.2",
    author="Ines Sanchez, Willie Kasakula, Hamed Farahani, Aldana Arruti Gamaldi",
    author_email="author@example.com",
    description="Multi-body gravitational simulation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ictp-cssd-2019/a2_planets",
    packages=['a2_planets'], #setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_data={
     '': ['*.txt'],
     'mypkg': ['examples/*.txt'],
    },
    scripts=['plotplanets'],
)
