import configparser
import math
from .planet import Planet

planets = []
G = 0


def import_planets(config_file):
    """
        Reads the user config file
        Initializes all the planet specifications
        Loads all the Planet objects to the planets List
    """
    config = configparser.ConfigParser()
    config.read(config_file)

    global G

    for section in config.sections():
        if section == 'Global':
            G = float(config['Global']['G'])
        else:
            planet_config = config[section]
            name = planet_config['name']
            mass = float(planet_config['mass'])
            x = float(planet_config['x_0'])
            y = float(planet_config['y_0'])
            vx = float(planet_config['vx_0'])
            vy = float(planet_config['vy_0'])

            planets.append(Planet(x, y, vx, vy, mass, name))


def change_position(planet, step_time):
    """
    change_position function calculates a planet's position.

    Parameters:
    Planet: (object) Contains the planet's information.

    Returns:
    vx,vy: (tuple) x and y components of the planet's position.
    """
    planet.x = planet.x + planet.vx * step_time + (0.5 * planet.ax * step_time ** 2)
    planet.y = planet.y + planet.vy * step_time + (0.5 * planet.ay * step_time ** 2)
    string = """
        name: {}
        x: {}
        y: {}
    """.format(planet.name, planet.x, planet.y)
    # print(string)
    return planet.x, planet.y


def change_velocity_partially(planet, step_time):
    """
    change_velocity function calculates a planet's velocity.

    Parameters:
    Planet: (object) Contains the planet's information.

    Returns:
    vx,vy: planet's velocity.
    """
    planet.vx = planet.vx + (0.5 * planet.ax * step_time)
    planet.vy = planet.vy + (0.5 * planet.ay * step_time)
    return planet.vx, planet.vy


def update_velocity(planet, step_time):
    planet.vx += 0.5 * planet.ax * step_time
    planet.vy += 0.5 * planet.ay * step_time


def get_distance(planet1, planet2):
    """
        -The universal gravitation law states that the attraction is a function of the distance between the planets.
        In this function is calculated the distance between two planets, as a function of their positions X and Y

        -Parameters:
        Planet:(object). X and Y are variables that describe the position on 2D space dimension for each planet

        -Returns:
        dx, dy: (tuple) The function returns the distance between the planets, as components (coordinates) x and y on the space.
    """

    x1, y1 = planet1.x, planet1.y
    x2, y2 = planet2.x, planet2.y
    dx = (x2 - x1)
    dy = (y2 - y1)
    distance = math.sqrt((dx * dx) + (dy * dy))
    return dx, dy, distance


def forces_fun(planet1, planet2, G):
    """
        In this function is resolved the Newton's law of universal gravitation. This function finds the attraction force between two planest as the product of their masses and inversely proportional to the square of the distance between their centres.
        Parameters:
        Planet:(object) Masses from the planets (m_1 and m_2), as value included by the user. The distance between planets (x and y) are calculated from distance function.
        G:(float) Cavendish constant. 6.74×10−11 m3 kg–1 s−2.
        Returns:
        force_x, force_y: (tuple) The function returns the attraction force components x and y for the planets system.
    """

    dx, dy, distance = get_distance(planet1, planet2)

    # if dx == 0:
    #    theta = math.pi/2
    # else:
    #    theta=math.atan(dy/dx)

    m_1 = planet1.m
    m_2 = planet2.m

    force_x = (G * ((m_1 * m_2) / (distance ** 3))) * (dx)
    force_y = (G * ((m_1 * m_2) / (distance ** 3))) * (dy)

    # force_x = force* math.cos(theta)
    # force_y = force* math.sin(theta)

    return force_x, force_y


def acceleration_fun(Planets, G):
    """
        Universally, the Force that is perceived by an object, is the result of the change in the velocity (acceleration) per object mass. Consequently, the acceleration function is the uses the attraction forces calculated on the forces_fun and each planet mass.

        Parameters:
        Planet:(object) Attraction force components x and y, for each planet
        G:(float) Cavendish constant. 6.74×10−11 m3 kg–1 s−2.

        Returns:
        ax,ay: (tuple) The function returns the components x and y of the change in the velocity (acceleration) for each planet.
    """

    for planet in Planets:
        forces_x = 0
        forces_y = 0
        rest_planets = Planets.copy()
        rest_planets.remove(planet)
        for otherplanets in rest_planets:
            forces_x += forces_fun(planet, otherplanets, G)[0]
            forces_y += forces_fun(planet, otherplanets, G)[1]
            # print(forces_x, forces_y)
        planet.ax = forces_x / planet.m
        planet.ay = forces_y / planet.m


"""for i in range(10):
    import_planets()
    acceleration_fun(planets, G)
    for planet in planets:
        change_velocity(planet)
        change_position(planet)
"""
