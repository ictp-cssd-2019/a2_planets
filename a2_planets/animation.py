import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from a2_planets import functions

g_planets = []
step_time = 0.003


def setup_animation(config_file):
    global figure, writer, xs, ys, scatterplot
    # import all planets from the user config file
    functions.import_planets(config_file)
    # define the figure hierarchy (figure holds axes)
    figure = plt.figure()
    axes = figure.add_subplot('111', aspect='equal')
    axes.set_xlim(-1.5, 1.5)
    axes.set_ylim(-1.5, 1.5)
    # Set up formatting for the movie files
    '''Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
    # Load all the planets from the main file to create graphical objects'''
    colors = ["red", "blue", "black", "orange"]
    xs = []
    ys = []
    cols = []
    i = 0
    j = 0
    for p in functions.planets:
        planet = plt.Circle((p.x, p.y), radius=0.05, color=colors[j])
        xs.append(p.x)
        ys.append(p.y)
        cols.append(colors[j])
        g_planets.append(planet)
        axes.add_patch(planet)
        i += 1
        j = i % 4
    scatterplot = plt.scatter(xs, ys, c=cols, linewidths=0.0005)


#
# xs.append() ys.append()
# scatterplot.update_data(xs,ys,cols=cols)

def animate(i):
    # shift the planets' position
    """
    Calculate planets acceleration
    Change the planets velocity
    Move the planets according to the change in position
    """

    planets = functions.planets
    for a in range(len(planets)):
        functions.change_velocity_partially(planets[a], step_time)

    functions.acceleration_fun(functions.planets, functions.G)

    for a in range(len(planets)):
        functions.update_velocity(planets[a], step_time)
        functions.change_position(planets[a], step_time)
        xs.append(planets[a].x)
        ys.append(planets[a].y)

        # if i % 20 == 0:
        g_planets[a].center = (planets[a].x, planets[a].y)
        scatterplot.set_offsets(np.c_[xs, ys])

        # print(planets[a].name, planets[a].x, planets[a].y)


# afterwards, switch to zoomable GUI mode

def simulate(config_file):
    setup_animation(config_file)
    ani = animation.FuncAnimation(figure,
                                  animate,
                                  np.arange(1, 5000),
                                  interval=25)
    # ani.save('animation5.mp4', writer=writer)
    plt.show()
