class Planet:
    """
    Planet class contains the planet's information.

    Attributes:
    x (float) x component of planet's position.
    y (float) y component of planet's position.
    vx (float) x component of planet's velocity.
    vy (float) y component of planet's velocity.
    ax (float) x component of planet's position.
    ay (float) y component of planet's position.
    m (float) planet's mass.
    name (string) planet's name.

    """
    def __init__(self,x,y,vx,vy,m,name):
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
        self.m = m
        self.name = name
        self.ax = 0
        self.ay = 0
