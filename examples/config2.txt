[Global]
G = 1

[Planet_1]
name = Earth
mass = 1
x_0 = 0
y_0 = -1
vx_0 = 0.39295
vy_0 = 0.09758

[Planet_2]
name = Jupiter
mass = 1
x_0 = 0
y_0 = 1
vx_0 = 0.39295
vy_0 = 0.09758

[Planet_3]
name = S
mass = 1
x_0 = 0
y_0 = 0
vx_0 = -0.7859
vy_0 = -1.9516
