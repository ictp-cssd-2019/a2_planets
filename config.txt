[Global]
G = 1

[Planet_1]
name = Earth
mass = 30
x_0 = -0.5
y_0 = 0
vx_0 = 0
vy_0 = 3

[Planet_2]
name = Jupiter
mass = 30
x_0 = 0.5
y_0 = 0
vx_0 = 0
vy_0 = -3
